<?php

use Phalcon\Mvc\model;

class Users extends Model {
  protected $uid;
  public $firstname;
  public $lastname;
  public $document;
  public $document_type;
  public $email;
  public $address;
  public $id_pnr;

  public function getSource() {
    return 'users';
  }

  public function getUid() {
    return $this->uid;
  }

  public function insertUser($firstname, $lastname, $document, $document_type, $email, $address, $id_pnr = NULL) {
    $this->firstname = $firstname;
    $this->lastname = $lastname;
    $this->document = $document;
    $this->document_type = $document_type;
    $this->email = $email;
    $this->address = $address;
    $this->id_pnr = $id_pnr;

    return $this->save();
  }

}
