<?php

use Phalcon\Mvc\Model;

class WsLog extends Model
{
	protected $lid;

	public $type;

	public $method;

	public $params;

	public $result;

	public $timestamp;


  public function getSource()
  {
    return "ws_log";
  }

  public function getLid() {
    return $this->lid;
  }

  public function insertLog($log_type, $log_method, $log_params, $log_result) {
    $this->type      = $log_type;
    $this->method    = $log_method;
    $this->params    = $log_params;
    $this->result    = $log_result;
    $this->timestamp = time();

    $save_status =  $this->save();
    if($save_status) {
      return 'Insert log sucessful';
    } else {
      return 'Insert log fail';
    }
  }
}
