<?php

use Phalcon\Mvc\Model;

class CustomerRoles extends Model {

  public $cid;
  public $rid;
  public $customers_cid;
  public $customers_username;

  public function getSource() {
    return 'customers_roles';
  }

  public function insertCustomerRole($cid, $rid, $customers_cid, $customers_username) {
    $this->cid = $cid;
    $this->rid = $rid;
    $this->customers_cid = $customers_cid;
    $this->customers_username = $customers_username;
    $save_status =  $this->save();
    if($save_status) {
      return 'Insert Customer role sucessful';
    } else {
      return 'Insert Customer role fail';
    }

  }
}
