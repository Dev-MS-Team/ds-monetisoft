<?php

use Phalcon\Mvc\model;

class Customer extends Model {
  protected $cid;
  public $username;
  public $api_key;
  public $token;
  public $token_expiration;

  public function getSource() {
    return 'customers';
  }

  public function getCid() {
    return $this->cid;
  }

  public function insertCustomer($username, $api_key, $token, $token_expiration) {
    $this->username = $username;
    $this->api_key = $api_key;
    $this->token = $token;
    $this->token_expiration = $token_expiration;

    $save_status =  $this->save();
    if($save_status) {
      return 'Insert Customer sucessful';
    } else {
      return 'Insert Customer fail';
    }
  }

}
