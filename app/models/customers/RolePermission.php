<?php

use Phalcon\Mvc\Model;

class RolePermission extends Model {

  public $rid;
  public $permission;
  public $role_rid;

  public function getSource() {
    return 'role_permission';
  }

}
