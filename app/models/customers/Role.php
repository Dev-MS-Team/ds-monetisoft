<?php

use Phalcon\Mvc\Model;

class Role extends Model {
  protected $rid;
  public $name;
  public $customers_roles_cid;
  public $customers_roles_rid;

  public function getRid() {
    return $this->rid;
  }

  public function getSource() {
    return 'role';
  }

  public function insertRole($name, $customers_roles_cid, $customers_roles_rid) {
    $this->name = $name;
    $this->customers_roles_cid = $customers_roles_cid;
    $this->customers_roles_rid = $customers_roles_rid;
    $save_status =  $this->save();
    if($save_status) {
      return 'Insert Role sucessful';
    } else {
      return 'Insert Role fail';
    }
  }
}
