<?php

use Phalcon\Mvc\Model;

class SabreSession extends Model
{
	protected $sid;

	public $request_type;

	public $token_auth;

	public $token_expiration;

	public $created;


    public function getSource()
    {
        return "sabre_session";
    }

    public function getSid() {
    	return $this->sid;
    }
}