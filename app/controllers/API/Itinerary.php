<?php 

class Itinerary {
  
  protected $request;

  function __construct($request_info) {
  	$this->request = $request_info;
  }

  /**
  * CANCEL ITINERARY BOOK 
  */

  /**
  * function CancelItineraryBook
  * used to cancel a itinerary segment book
  */
  public function CancelItineraryBook() {
    $output = array();
    //get vars
    $vars = $this->request->getPost();
    if (!isset($vars['document']) || empty($vars['document'])) {
      $output['status'] = FALSE;
      $output['message'] = 'unknown document number';
      return print json_encode($output);
    }

    if (!isset($vars['document_type']) || empty($vars['document_type'])) {
      $output['status'] = FALSE;
      $output['message'] = 'unknown document type';
      return print json_encode($output);
    }

    //find in users, user with received document info
    $users = Users::find(array(
        'conditions' => 'document = "'.$vars['document'].'" AND document_type = "'.$vars['document_type'].'"'
      ));

    if (count($users) > 1) {
      //too many users with same number identification
      $output['status'] = FALSE;
      $output['message'] = 'unexpected error';
      return print json_encode($output);

    } elseif (!count($users)) {
      $output['status'] = FALSE;
      $output['message'] = 'User does not exist';
      return print json_encode($output);
    }

    /*
    * User exist
    */
    if (count($users) == 1) {
      $user = $users->getFirst();
      if (isset($user->id_pnr) && !empty($user->id_pnr)) {
        //Sabre user
        //set PNR to vars
        $vars['unique_id'] = $user->id_pnr;
        $sabre_itinerary = new SabreItinerary();
        $response = $sabre_itinerary->sabreWorkflowCancelBook($vars);
        $output['results'] = $this->getSabreCancelBookResults($response);
      }
    }

    echo json_encode($output);

  }

  /**
  * function getSabreCancelBookResults()
  * used to convert sabreWorkflowCancelBook response to DS API friendly response
  * @param (response) $response: sabreWorkflowCancelBook ws response
  */
  protected function getSabreCancelBookResults($response) {
    if (isset($response->ApplicationResults->Success)) {
      //success
      $output['status'] = TRUE;
      $output['message'] = 'Success';
      $output['text'] = array();
      $output['text'] = isset($response->Text) ? $response->Text : 'NO ITIN';  
    } else {
      //error on request
      $output['status'] = FALSE;
      $output['message'] = 'Request error';
    }

    return $output;
  }

  /**
  * END CANCEL ITINERARY BOOK 
  */


}