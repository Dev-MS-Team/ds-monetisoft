<?php

class Hotel {

  protected $request;

  function __construct($request_info) {
    $this->request = $request_info;
  }

  /**
  * HOTEL AVAILABILITY
  * callback of '/hotels/avail', return hotel availability options
  */
  public function HotelAvailability() {
    $output = array();
    //get vars
    $vars = $this->request->getPost();
    if (!isset($vars['guests']) || empty($vars['guests'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown guests number';
      return print json_encode($output);
    }
    if (!isset($vars['latitude']) || empty($vars['latitude'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown latitude';
      return print json_encode($output);
    }
    if (!isset($vars['longitude']) || empty($vars['longitude'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown longitude';
      return print json_encode($output);
    }
    if (!isset($vars['end_date']) || empty($vars['end_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown end date';
      return print json_encode($output);
    }
    if (!isset($vars['start_date']) || empty($vars['start_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown start date';
      return print json_encode($output);
    }

    if (!$this->validateDates($vars['start_date'], $vars['end_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'Check dates';
      return print json_encode($output);
    }

    //$output['status'] = TRUE;
    $sabre_hotel = new SabreHotel();
    $response  = $sabre_hotel->sabreWorkflowSearchHotel($vars);
    $output['Results'] = $this->getSabreHotelResults($response);
    echo json_encode($output);
  }

  /**
  * function getSabreHotelResults()
  * mapped sabre response fields from HotelAvailability call
  * @param (array) $response: sabre response Object
  * @return (array) $output: array mapped of fields used to API DS
  */
  protected function getSabreHotelResults($response) {
    $output = array();

    if (isset($response->ApplicationResults->Success)) {
      //success
      $output['Status'] = TRUE;
      $output['Message'] = 'Success';
      $output['Options'] = array();
      if (!empty($response->AvailabilityOptions->AvailabilityOption)) {
        $avail_options = $response->AvailabilityOptions->AvailabilityOption;
        if (is_array($avail_options)) {
          //multiple options
          foreach ($avail_options as $key => $value) {
            $output['Options'][] = array(
              'ChainCode' => $value->BasicPropertyInfo->ChainCode,
              'HotelCode' => $value->BasicPropertyInfo->HotelCode,
              'HotelCityCode' => $value->BasicPropertyInfo->HotelCityCode,
              'HotelName' => $value->BasicPropertyInfo->HotelName,
              'Address' => $value->BasicPropertyInfo->Address->AddressLine[0],
              'ContactNumbers' => $value->BasicPropertyInfo->ContactNumbers,
              'Location' => $value->BasicPropertyInfo->LocationDescription,
              'Rating' => $value->BasicPropertyInfo->Property,
              'PropertyInfo' => $value->BasicPropertyInfo->PropertyOptionInfo,
              'SpecialOffers' => $value->BasicPropertyInfo->SpecialOffers,
              'RoomRate' => $value->BasicPropertyInfo->RoomRate,
              'RateRange' => $value->BasicPropertyInfo->RateRange,
            );
          }

        } else {
          //single option
          $output['Options'] = array(
            'ChainCode' => $avail_options->BasicPropertyInfo->ChainCode,
            'HotelCode' => $avail_options->BasicPropertyInfo->HotelCode,
            'HotelCityCode' => $avail_options->BasicPropertyInfo->HotelCityCode,
            'HotelName' => $avail_options->BasicPropertyInfo->HotelName,
            'Address' => $avail_options->BasicPropertyInfo->Address->AddressLine[0],
            'ContactNumbers' => $avail_options->BasicPropertyInfo->ContactNumbers,
            'Location' => $avail_options->BasicPropertyInfo->LocationDescription,
            'Rating' => $avail_options->BasicPropertyInfo->Property,
            'PropertyInfo' => $avail_options->BasicPropertyInfo->PropertyOptionInfo,
            'SpecialOffers' => $avail_options->BasicPropertyInfo->SpecialOffers,
            'RoomRate' => $avail_options->BasicPropertyInfo->RoomRate,
            'RateRange' => $avail_options->BasicPropertyInfo->RateRange,
          );
        }
      }

    } else {
      //error on request
      $output['Status'] = FALSE;
      $output['Message'] = 'Request error';
    }

    return $output;
  }

  /** END HOTEL AVAIL RESPONSE**/


  /**
  * HOTEL PROPERTY
  * calback of '/hotels/hotel-properties', return characteristics of a hotel
  */
  public function HotelProperty() {
    $output = array();
    //get vars
    $vars = $this->request->getPost();
    if (!isset($vars['guests']) || empty($vars['guests'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown guests number';
      return print json_encode($output);
    }
    if (!isset($vars['hotel_code']) || empty($vars['hotel_code'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown hotel code';
      return print json_encode($output);
    }
    if (!isset($vars['end_date']) || empty($vars['end_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown end date';
      return print json_encode($output);
    }
    if (!isset($vars['start_date']) || empty($vars['start_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown start date';
      return print json_encode($output);
    }

    if (!$this->validateDates($vars['start_date'], $vars['end_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'Check dates';
      return print json_encode($output);
    }

    $sabre_hotel = new SabreHotel();
    $response  = $sabre_hotel->sabreWorkflowHotelProperty($vars);
    //print_r($response);
    $output['Results'] = $this->getSabreHotelPropertyResults($response);
    echo json_encode($output);
  }

  protected function getSabreHotelPropertyResults($response) {
    if (isset($response->ApplicationResults->Success)) {
      //success
      $output['Status'] = TRUE;
      $output['Message'] = 'Success';
      $output['PropertyInfo'] = array();
      $output['PropertyInfo'] = $response->RoomStay;
    } else {
      //error on request
      $output['Status'] = FALSE;
      $output['Message'] = 'Request error';
    }

    return $output;
  }

  /**
  * END HOTEL PROPERTY
  */



  /**
  * HOTEL RATE DESCRIPTION
  * calback of '/hotels/hotel-rate-description', return rate info of property
  */
  public function HotelRateDescription() {
    $output = array();
    //get vars
    $vars = $this->request->getPost();
    if (!isset($vars['guests']) || empty($vars['guests'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown guests number';
      return print json_encode($output);
    }
    if (!isset($vars['hotel_code']) || empty($vars['hotel_code'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown hotel code';
      return print json_encode($output);
    }
    if (!isset($vars['end_date']) || empty($vars['end_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown end date';
      return print json_encode($output);
    }
    if (!isset($vars['start_date']) || empty($vars['start_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown start date';
      return print json_encode($output);
    }
    if (!isset($vars['rph']) || empty($vars['rph'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown rph code';
      return print json_encode($output);
    }

    if (!$this->validateDates($vars['start_date'], $vars['end_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'Check dates';
      return print json_encode($output);
    }

    $sabre_hotel = new SabreHotel();
    $response  = $sabre_hotel->sabreWorkflowHotelRateDescription($vars);

    $output['results'] = $this->getSabreHotelRateResults($response);
    echo json_encode($output);

  }

  protected function getSabreHotelRateResults($response) {
    if (isset($response->ApplicationResults->Success)) {
      //success
      $output['Status'] = TRUE;
      $output['Message'] = 'Success';
      $output['RateInfo'] = array();
      $output['RateInfo'] = $response->RoomStay;
    } else {
      //error on request
      $output['Status'] = FALSE;
      $output['Message'] = 'Request error';
    }

    return $output;
  }

  /**
  * END HOTEL RATE DESCRIPTION
  */


  /**
  * function validateDates
  * used to validate format dates and compare with a current date in MM-DD format
  * @param (string) $start_date: check-in Date  MM-DD
  * @param (string) $end_date: check-out Date MM-DD
  * @return (bool)
  */
  public function validateDates($start_date, $end_date) {
    $pattern = '/^(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/';
    if (preg_match($pattern, $start_date) && preg_match($pattern, $end_date)) {
      $current  = date('m-d');
      if ($end_date >= $current && $start_date >= $current && $end_date >= $start_date ) {
        return TRUE;
      }
    }

    return FALSE;

  }

  /**
   * Function hotelBooking()
   * callback from /hotels/hotel-booking
   * @return string json encode array result of booking
   */

  public function HotelBooking() {
    $vars = $this->request->getPost();
    //Validate params
    if (!isset($vars['guests']) || empty($vars['guests'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown guests number';
      return print json_encode($output);
    }
    if (!isset($vars['hotel_code']) || empty($vars['hotel_code'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown hotel code';
      return print json_encode($output);
    }
    if (!isset($vars['end_date']) || empty($vars['end_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown end date';
      return print json_encode($output);
    }
    if (!isset($vars['start_date']) || empty($vars['start_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown start date';
      return print json_encode($output);
    }

    if (!isset($vars['number_units']) || empty($vars['number_units']) || !is_numeric($vars['number_units'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown number of units';
      return print json_encode($output);
    }
    if (!isset($vars['ps_phone']) || empty($vars['ps_phone'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown phone';
      return print json_encode($output);
    }
   /* if (!isset($vars['ps_phone_use_type']) || empty($vars['ps_phone_use_type'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown phone use type';
      return print json_encode($output);
    }*/
    if (!isset($vars['ps_givenname']) || empty($vars['ps_givenname'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown givenname';
      return print json_encode($output);
    }
    if (!isset($vars['ps_surname']) || empty($vars['ps_surname'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown Surname';
      return print json_encode($output);
    }
    if (!isset($vars['ps_document']) || empty($vars['ps_document'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown document';
      return print json_encode($output);
    }
    if (!isset($vars['ps_document_type']) || empty($vars['ps_document_type'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown document type';
      return print json_encode($output);
    }
    if (!isset($vars['ps_address']) || empty($vars['ps_address'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown address';
      return print json_encode($output);
    }
    if (!isset($vars['ps_email']) || empty($vars['ps_email'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'unknown email';
      return print json_encode($output);
    }
    if (!$this->validateDates($vars['start_date'], $vars['end_date'])) {
      $output['Status'] = FALSE;
      $output['Message'] = 'Check dates';
      return print json_encode($output);
    }

    $sabre_hotel = new SabreHotel();
    $response = $sabre_hotel->sabreWorkflowHotelReservation($vars);
    $output['Results'] = $this->getSabreHotelBookingResult($response);
    return print json_encode($output);
  }


  protected function getSabreHotelBookingResult($response) {
    if (isset($response['Reservation']->ApplicationResults->Success) && isset($response['UserTransaction']->ApplicationResults->Success)) {
      $output['Status'] = TRUE;
      $output['Message'] = 'Success';
      $output['ReservationData'] = $response['Reservation'];
      $output['TransactionData'] = $response['UserTransaction'];
    } else {
      $output['Status'] = FALSE;
      $output['Message'] = 'Reservation not processed';
      $output['ReservationData'] = $response['Reservation'];
      $output['TransactionData'] = $response['UserTransaction'];
    }
    return $output;
  }



}
