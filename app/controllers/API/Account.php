<?php



class Account {

  private $request;

  function __construct($request) {
    $this->request = $request;
  }

  /**
   * Function generateToken()
   * Callback from /auth/generate/token
   * calls set token for generate the authentication token.
   * @return string customer token.
   */

  public function generateToken() {
    $request = $this->request->getHeaders();
    if (isset($request['REFRESH_TOKEN']) && !empty($request['REFRESH_TOKEN']) ) {
      $result = $this->validateRefreshToken($request['REFRESH_TOKEN']);
      if ($result['access'] == TRUE) {
        $response = $this->setToken($result['result']['username'], $result['result']['api_key']);
      } else {
        $response = $result;
      }
    } else {
      $response = $this->setToken($request['USERNAME'], $request['API_KEY']);
    }
    return print json_encode($response);
  }

  /**
   * Function setToken()
   * @param string $username
   * @param string $api_key
   * @return string $token
   */
  protected function setToken($username, $api_key) {

    $model_customer = new Customer();
    $query = $model_customer::find(array(
      'conditions' => "username = '" . $username . "' AND api_key = '" . $api_key . "'",
    ));
    $result = $query->getLast();
    if(isset($result->username, $result->api_key)) {
      $token_create = time();
      $token_expiration = $token_create + 604800;
      $expire = strrev($token_expiration);
      $chain = 'DS:' . base64_encode($username) . ':' . base64_encode($api_key) . ':' . $expire;
      $token = base64_encode(strrev($chain));
      $result->token = $token;
      $refresh_token = $this->generateRefreshToken($token, $token_create, $result->getCid());
      $result->token_expiration = $token_expiration;
      $save_status = $result->save();
      if($save_status) {
        return array('Token' => $token, 'Expiration' => $token_expiration, 'RefreshToken' => $refresh_token);
      } else {
        return array('Error' => 'Token Create Fail');
      }
    } else {
      return array('Error' => 'Incorrect params');
    }

  }

  /**
   * Function validateToken()
   * This function get token from header, and decode to validate.
   * @return array response access.
   */
  public function validateToken() {
    $header = $this->request->getHeaders();
    $output = $this->getToken($header['TOKEN']);
    return $output;
  }

  /**
   * Function getToken()
   * Function that return and validate user token.
   * @param string $token
   * @return array
   */
  public function getToken($token) {
    $customer = new Customer();
    $query = $customer::find(array(
        'conditions' => "token = '" . $token . "'",
      )
    );
    $result = $query->getLast();
    if (isset($result->token) && $result->token === $token) {
      $decode_token = $this->decodeToken($token);
      if ($result->username !== $decode_token['username'] || $result->api_key !== $decode_token['api_key'] ) {
        return array('access' => FALSE, 'result' => 'No valid token');
      }
      if (time() > $result->token_expiration) {
        //token invalid
        return array('access' => FALSE, 'result' => 'Token has expired');
      }
      return array('access' => TRUE, 'result' => $result->username);
    } else {
      return array('access' => FALSE, 'result' => 'Access denied');
    }
  }

  /**
   * Function decodeToken()
   * Function that decode the string token
   * @param string $token
   * @return array array with customer data, obtained in the token.
   */
  protected function decodeToken($token) {
    $string_token = strrev(base64_decode($token));
    $array_token = explode(':', $string_token);
    $len = strlen($array_token[1]);
    $result = array(
      'username' => base64_decode($array_token[1]),
      'api_key' => base64_decode($array_token[2]),
      'token_expiration' => strrev($array_token[3]),
    );
    return $result;
  }

  protected function validateRefreshToken($r_token) {

    $info_token = $this->decodeRefreshToken($r_token);

    $customer = new Customer();
    $query = $customer::find(array(
        'conditions' => "token = '" . $info_token['token'] . "' AND cid='" . $info_token['cid'] . "'",
      )
    );
    $result = $query->getLast();
    if (isset($result->token) && $result->token === $info_token['token']) {
      if ($result->username !== $info_token['username'] || $result->api_key !== $info_token['api_key'] ) {
        return array('access' => FALSE, 'result' => 'No valid token');
      }
      if ($result->token_expiration == ($info_token['token_create'] + 604800)) {
        return array('access' => TRUE, 'result' => $info_token);
      }
    } else {
      return array('access' => FALSE, 'result' => 'Access denied');
    }

  }

  protected function decodeRefreshToken($r_token) {
    $string = base64_decode($r_token);
    $array_parts = explode('//-', $string);
    $token = $array_parts[1];
    $deco_token = $this->decodeToken($token);
    $result = array(
      'cid' => hex2bin($array_parts[0]),
      'token' => $token,
      'token_create' => strrev(hex2bin($array_parts[2])),
    );

    return array_merge($result, $deco_token);

  }

  protected function generateRefreshToken($token, $token_create, $cid) {
    $len = strlen($token);
    $signal = '//-';
    $r_token = base64_encode(bin2hex($cid) . $signal . $token . $signal . bin2hex(strrev($token_create)));
    return $r_token;
  }


}
