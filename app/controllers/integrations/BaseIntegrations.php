<?php

define("GET", "GET");
define("POST", "POST");

class BaseIntegrations {

  function __construct($service) {
    $this->service = $service;
    $this->security_header = NULL;
  }

  private function setConfig() {
    //instanciar clase de configuraciones para cada servicio REST y SOAP por integración.
  }

  /******************************************************
   *                   SOAP                             *
   ******************************************************/


  /**
   * Function soap_instance create a instance of SoapClient()
   * @param string $url_wsdl url string of wsdl, or null if is no-wsdl
   * @param array $options Array of options for request
   * @return Object SoapClient
   */
  public function soapInstance($url_wsdl, $options) {
    $client = new SoapClient($url_wsdl, $options);
    $this->client = $client;
    return $client;
  }

  /**
   * Function soap_instance create a instance of SoapClient()
   * @param string $dev_key
   * @param string $password
   * @param string $namespace
   * @param string $name
   * @param array $data
   * @return Object $SoapClient
   */

  public function createSoapHeader($devkey = '', $password = '', $namespace, $name , $data) {
    // Create the header

    $data   = !empty($devkey) ? new ChannelAdvisorAuth($devkey, $password) : $data;
    $header = new SoapHeader($namespace, $name, $data, false);
    return $header;
  }
  /**
   * function createSecurityHeader
   * @param array security_array
   * @return Object SoapHeader
   */
  public function createSecurityHeader($security_array) {
      $security_header = new SoapHeader("http://schemas.xmlsoap.org/ws/2002/12/secext", "Security", $security_array, 1);
      $this->security_header = $security_header;
      return $security_header;
  }


  public function callSoap($function_name, $arguments, $options, $headers = NULL, $output_headers = NULL ) {
    $this->client->__soapCall ( $function_name ,
     $arguments, $options, array($headers, $this->security_header), $output_headers);
  }

  /******************************************************
   *                   REST                             *
   ******************************************************/

  /**
   * Function executeGetCall
   * @param string $path
   * @param array $params
   * @return array json_decode
   */

  public function executeGetCall($path, $params, $headers) {
    $result = curl_exec($this->prepareCall(GET, $path, $params, $headers));
    return json_decode($result);
  }

  /**
   *  Function executePostCall()
   * @param string $path
   * @param array $params
   * @return array json_decode
   */

  public function executePostCall($path, $params, $headers = array()) {
    $result = curl_exec($this->prepareCall(POST, $path, $params, $headers));
    return json_decode($result);
  }

  /**
   * Function prepareCall
   * @param $call_type String HTTP method
   * @param $path      String path
   * @param $params   array params
   * @param $headers   array header
   *
   * @return handler cURL
   */

  private function prepareCall($call_type, $path, $params, $headers) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $call_type);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    switch ($call_type) {
    case GET:
        $url = $path;
        if ($params != null) {
            $url = $url . '?' . http_build_query($params);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        break;
    case POST:
        curl_setopt($ch, CURLOPT_URL, $path);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        if (!empty($headers)) {
          array_push($headers, 'Content-Type : application/json');
        }
        break;
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    return $ch;
  }

}

?>
