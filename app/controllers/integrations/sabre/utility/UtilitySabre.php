<?php

  class UtilitySabre extends Sabre{

    function __construct() {
      parent::__construct();
    }


    /**
   * function sabreGoAutocomplete()
   * Funtion returns a list of location predictions for text-based geographic search queries.
   * @param string $query text for search
   * @param string $category AIR for Airports or CITY for Cities or RAIL for Rail stations.
   * @param int $limit the number of search results, valid 1 to 30.
   *
   */

  public function sabreGoAutocomplete($query = '', $category = 'CITY', $limit = 5) {
    $path = $this->environment . '/v1/lists/utilities/geoservices/autocomplete';
    $params = array(
      'query'    => $query,
      'category' => $category,
      'limit'    => $limit,
    );
    $headers = $this->buildAuthHeader();
    if ($query != '') {
      $output = $this->executeGetCall($path, $params, $headers);
    }
    return $output;
  }


  }

?>
