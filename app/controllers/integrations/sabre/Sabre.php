<?php


class Sabre extends BaseIntegrations {

  private $username;

  private $password;

  private $organization;

  private $domain;

  protected $environment;

  function __construct() {
    $config = parse_ini_file('sabre.ini');
    //REST CREDENTIALS
    $this->environment   = $config['REST']['environment'];
    $this->user_id       = $config['REST']['user_id'];
    $this->group         = $config['REST']['group'];
    $this->rest_domain        = $config['REST']['domain'];
    $this->client_secret = $config['REST']['client_secret'];
    //SOAP CREDENTIALS
    $this->username     = $config['SOAP']['Username'];
    $this->password     = $config['SOAP']['Password'];
    $this->organization = $config['SOAP']['Organization'];
    $this->domain       = $config['SOAP']['Domain'];
  }

  /**
   * function buildAuthHeader()
   * Return the authentication header
   * @return Authentication header.
   */

  protected function buildAuthHeader() {
    $token = $this->sabreGetTokenAuth();
    $headers = array(
      'Authorization: Bearer' . $token,
    );
    return $headers;
  }

  /**
   * function buildCredentials()
   * Return the encoded credentials for sabre.
   * @return string Encoded credentials.
   */

  private function buildCredentials() {
    $credentials =  'V1' . ":" .
      $this->user_id . ":" .
      $this->group . ":" .
      $this->rest_domain;
    $secret = base64_encode($this->client_secret);
    return base64_encode(base64_encode($credentials).":".$secret);
  }

  /**
   * Function authenticationSabre()
   *
   * @return string authentication token.
   */

  public function authenticationSabre() {
    $path = $this->environment . "/v2/auth/token";
    $headers = array(
      'Authorization: Basic '. $this->buildCredentials(),
      'Accept: */*',
      'Content-Type: application/x-www-form-urlencoded'
    );
    $vars = "grant_type=client_credentials";
    $result = $this->executePostCall($path, $vars, $headers);
    $model = new SabreSession();
    $model->request_type     = 'REST';
    $model->token_auth       = $result->access_token;
    $model->token_expiration = $result->expires_in;
    $model->created          = time();
    $model->save();
    return $result->access_token;
  }

  /**
  * function sabreGetTokenAuth
  * @param $type: method used for ws request
  * @return $token: token for sabre auth
  */
  private function sabreGetTokenAuth($type = 'REST') {
    $current_time = time();
    $token = '';
    if ($type == 'REST') {
      /** get REST token from DB for Sabre Integration **/
      //get last stored token
      $sabre_sess = SabreSession::find(array(
        'conditions' => 'request_type = "REST"'
      ));
      $sabre_sess = $sabre_sess->getLast();
      if ($sabre_sess) {
        $token_creation = $sabre_sess->created;
        //expiration timestamp
        $token_expiration = $sabre_sess->token_expiration + $token_creation;
        if ($current_time <= $token_expiration) {
          //token with lifetime
          $token = $sabre_sess->token_auth;
        } else {
          //one day to expire
          $token = $this->authenticationSabre();
        }
      } else {
        //no token information
        $token = $this->authenticationSabre();
      }
    }
    elseif ($type == 'SOAP') {
      /** get SOAP token from DB for Sabre Integration **/
      $sabre_sess = SabreSession::find(array(
        'conditions' => 'request_type = "SOAP"'
      ));
      $sabre_sess = $sabre_sess->getLast();
      if ($sabre_sess) {
        $token = $sabre_sess->token_auth;
      }
    }

    return $token;
  }


  /**
  * function sabreSoapCreateSession()
  * Function to create a Sabre soap token auth, used in every call to SOAP API
  * @return (Object) $result: contains BinarySecurityToken for auth
  */
  public function sabreSoapCreateSession() {
    $client = new SoapClient(
      __DIR__."/wsdl/SessionCreateRQ/SessionCreateRQ.wsdl",
      //"http://webservices.sabre.com/wsdl/sabreXML1.0.00/usg/SessionCreateRQ.wsdl",
      array(
        "uri" => 'https://sws3-crt.cert.sabre.com',
        "location" => 'https://sws3-crt.cert.sabre.com',
        "stream_encoding(stream)" => "utf-8",
        "trace" => true,
        'cache_wsdl' => WSDL_CACHE_NONE
      )
    );
    $responseHeaders = NULL;
    try {
      $client->__soapCall(
        "SessionCreateRQ",
        $this->sabreSoapSessionRequestBody("SessionCreateRQ"),
        null,
        array(
          $this->sabreSoapCreateMessageHeader("SessionCreateRQ"),
          $this->createSecurityHeader(
            array(
              "UsernameToken" => array(
                "Username" => $this->username,
                "Password" => $this->password,
                "Domain" => 'DEFAULT',
                "Organization" => $this->organization
              )
            )
          ),
        ),
        $responseHeaders
      );
      //print_r($client->__getLastRequestHeaders());
    } catch (SoapFault $e) {
        var_dump($e);
    }
    $result = $responseHeaders["Security"];
    return $result;
  }

  protected function sabreSoapSessionRequestBody($actionString) {
    $result = array($actionString => array(
      "POS" => array(
        "Source" => array(
            "PseudeCityCode" => $this->organization,
        )
      )
    ));
    return $result;
  }

  protected function sabreSoapCreateMessageHeader($actionString) {
    $messageHeaderXml = $this->sabreSoapGetMessageHeaderXml($actionString);
    $soapVar = new SoapVar($messageHeaderXml, XSD_ANYXML, null, null, null);
    return new SoapHeader("http://www.ebxml.org/namespaces/messageHeader", "MessageHeader", $soapVar, 1);
  }

  protected function sabreSoapGetMessageHeaderXml($actionString) {
    $messageHeader = array("MessageHeader" => array(
        "_namespace" => "http://www.ebxml.org/namespaces/messageHeader",
        "From" => array("PartyId" => "sample.url.of.sabre.client.com"),
        "To" => array("PartyId" => '123123'),
        "CPAId" => $this->organization,
        "ConversationId" => "SWS-Test-" . $this->organization,
        "Service" => $actionString,
        "Action" => $actionString,
        "MessageData" => array(
          "MessageId" => "1000",
          "Timestamp" => date("Y-m-d\TH:i:s\Z"),
          "TimeToLive" => date("Y-m-d\TH:i:s\Z")
        )
      )
    );
    return XMLSerializer::generateValidXmlFromArray($messageHeader);
  }

  /**
   * function sabreSoapCloseSession()
   * @param string $BinarySecurityToken Token of the session to close.
   * @return Object SoapClient Result.
   */

  public function sabreSoapCloseSession($BinarySecurityToken) {
      $client = new SoapClient(
        __DIR__."/wsdl/SessionCloseRQ/SessionCloseRQ.wsdl",
        //"http://webservices.sabre.com/wsdl/sabreXML1.0.00/usg/SessionCloseRQ.wsdl",
        array(
          "uri" => 'https://sws3-crt.cert.sabre.com',
          "location" => 'https://sws3-crt.cert.sabre.com',
          "stream_encoding(stream)" => "utf-8",
          "trace" => true,
          'cache_wsdl' => WSDL_CACHE_NONE)
      );
      //Security Header array
      $security_header = array(
        'BinarySecurityToken' => $BinarySecurityToken,
      );
      try {
        $result = $client->__soapCall(
          "SessionCloseRQ",
          $this->sabreSoapSessionRequestBody('SessionCloseRQ'),
          null,
          array(
            $this->sabreSoapCreateMessageHeader("SessionCloseRQ"),
            $this->createSecurityHeader($security_header)
          )
        );
      } catch (SoapFault $e) {
          var_dump($e);
      }
      return $result;
  }



}


?>
