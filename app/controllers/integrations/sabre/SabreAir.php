<?php

class SabreAir extends Sabre {

  function __construct() {

  }


  /**
  * function sabreFlightsTo()
  * returns 20 of the lowest published fares available for a given destination.
  * @param $destination:  Any valid 3-letter IATA code of the destination airport or multi-airport city (MAC) code
  * @param $country_code: 2-letter ISO 3166 country code
  * @return $output: json published fares
  */
  public function sabreFlightsTo($destination = 'BOG', $country_code = 'CO') {
    $output = '';
    //get headers
    $headers = $this->buildAuthHeader();
    $service_url = $this->environment . '/v1/shop/flights/cheapest/fares/'.$destination;
    $params = array(
      'pointofsalecountry' => $country_code
      );

    //exec get call from base class
    $output = $this->executeGetCall($service_url, $params, $headers);

    return $output;
  }

}
