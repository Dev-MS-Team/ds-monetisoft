<?php


class SabreHotel extends Sabre {

  /***************************** SEARCH ***************************************/

  /**
  * function sabreSoapHotelAvailability()
  * @param String $BinarySecurityToken: token auth for soap API call
  * @param Array $search_criteria: array with search data for request
  */
  public function sabreSoapHotelAvailability($BinarySecurityToken, $search_criteria) {
    $client = new SoapClient(
        "http://webservices.sabre.com/wsdl/tpfc/OTA_HotelAvailLLS2.2.1RQ.wsdl",
        array(
          "uri" => 'https://sws3-crt.cert.sabre.com',
          "location" => 'https://sws3-crt.cert.sabre.com',
          "stream_encoding(stream)" => "utf-8",
          "trace" => true,
          'cache_wsdl' => WSDL_CACHE_NONE)
      );
      //Security Header array
      $security_header = array(
        'BinarySecurityToken' => $BinarySecurityToken,
      );
      try {
        $result = $client->__soapCall(
          "OTA_HotelAvailRQ",
          $this->sabreSoapHotelAvailRequestBody('OTA_HotelAvailRQ', $search_criteria),
          null,
          array(
            $this->sabreSoapCreateMessageHeader("OTA_HotelAvailLLSRQ"),
            $this->createSecurityHeader($security_header)
          )
        );
      } catch (SoapFault $e) {
          var_dump($e);
      }
      return $result;
  }

  public function sabreSoapHotelAvailRequestBody($actionString, $search_criteria) {

    /*search criteria*/
    //guests: (int) guests numbers
    //city_name: (string) city name for search
    //country_code: (string) 2-letter ISO 3166 country code
    //postal_code: (int) numeric postal code
    //street: (string) used to search for a hotel based upon a particular street address
    //city_code: (string) IATA city code
    //hotel_code: (int) is used to specify a hotel property number
    //hotel_name: (string) used to search for a hotel name
    //chain_code: (string) code to identify and search for hotel chain code
    //end_date: (string) date in format MM-DD
    //start_date: (string) date in format MM-DD
    $result = array($actionString => array(
      "Version" => "2.2.1",
      "ReturnHostCommand" => TRUE,
      "AvailRequestSegment" => array(
        "GuestCounts" => array(
          "Count" => $search_criteria['guests'],
        ),
        "HotelSearchCriteria" => array(
          "NumProperties" => "30",
          "Criterion" => array(
            "Address" => array(
              "CityName" => $search_criteria['city_name'],
              "CountryCode" => $search_criteria['country_code'],
              "PostalCode" => $search_criteria['postal_code'],
              "StreetNmbr" => $search_criteria['street'],
            ),
            "HotelRef" => array(
              //"HotelCityCode" => $search_criteria['city_code'],
              // "HotelCode" => $search_criteria['hotel_code'],
              // "HotelName" => $search_criteria['hotel_name'],
              // "ChainCode" => $search_criteria['chain_code'],
              "Latitude" => round($search_criteria['latitude'], 2),
              "Longitude" => round($search_criteria['longitude'], 2),
            ),
          ),
        ),
        "TimeSpan" => array(
          "End" => $search_criteria['end_date'],
          "Start" => $search_criteria['start_date']
        ),
      )
    ));
    return $result;

  }

  /**
   * Function sabreHotelPropertyDescription()
   * @param (int)    $guest_count Number of guest
   * @param (string) $hotel_code Hotels code identification
   * @param (string) $end_time  Time in format MM-DD
   * @param (string) $start_time Time in format MM-DD
   * @param (string) $BinarySecurityToken Sabre autentication's Security token
   * @param (string) $currency_code
   * @param (int) $max Max rate range.
   * @param (int) $min Min rate range.
   * @return (array) Service response.
   */

  public function sabreHotelPropertyDescription($BinarySecurityToken, $guest_count, $hotel_code, $end_time, $start_time, $currency_code = '', $max = '', $min = '') {

    $XML_service = array(
      'HotelPropertyDescriptionRQ' => array(
        'Version' => '2.3.0',
        'AvailRequestSegment' => array(
          'GuestCounts' => array('Count' => $guest_count),
          'HotelSearchCriteria' => array(
            'Criterion' => array(
              'HotelRef' => array('HotelCode' => $hotel_code),
            ),
          ),
          'RatePlanCandidates' => array(
            'RateRange' => array(
              'CurrencyCode' => !empty($currency_code) ? $currency_code : NULL,
              'Max' => !empty($max) ? $max : NULL,
              'Min' => !empty($min) ? $min : NULL,
            ),
          ),
          'TimeSpan' => array(
            'End' => $end_time,
            'Start' => $start_time
          ),
        ),
      ),
    );


    $client = new SoapClient(
      'http://webservices.sabre.com/wsdl/tpfc/HotelPropertyDescriptionLLS2.3.0RQ.wsdl',
      array(
        'uri' => 'https://sws3-crt.cert.sabre.com',
        'location' => 'https://sws3-crt.cert.sabre.com',
        'stream_encoding(stream)' => 'utf-8',
        'trace' => true,
        'cache_wsdl' => WSDL_CACHE_NONE)
    );
    //Security Header array
    $security_header = array(
      'BinarySecurityToken' => $BinarySecurityToken,
    );
    try {
      $result = $client->__soapCall(
        'HotelPropertyDescriptionRQ',
        $XML_service,
        null,
        array(
          $this->sabreSoapCreateMessageHeader('HotelPropertyDescriptionLLSRQ'),
          $this->createSecurityHeader($security_header)
        )
      );
    } catch (SoapFault $e) {
      var_dump($e);
    }

    return $result;

  }

  /**
   * Function sabreHotelRateDescription()
   * This function must be call after call propertyDescription
   * @param (string) $BinarySecurityToken Sabre autentication's Security token
   * @param (int)    $rph Number of RPH code in propertyDescription
   * @return (array) Service response.
   */

  public function sabreHotelRateDescription($BinarySecurityToken, $rph) {
    $service_array = array(
      'HotelRateDescriptionRQ' => array(
          'xmlns' => 'http://webservices.sabre.com/sabreXML/2011/10',
          'xmlns:xs' => 'http://www.w3.org/2001/XMLSchema',
          'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
          'Version' => '2.3.0',
          'ReturnHostCommand' => 'false',
        'AvailRequestSegment' => array(
          'RatePlanCandidates' => array(
            'RatePlanCandidate' => array(
              'RPH' => $rph,
            ),
          ),
        ),
      ),
    );

    $client = new SoapClient(
      'http://wsdl-crt.cert.sabre.com/wsdl/tpfc/HotelRateDescriptionLLS2.3.0RQ.wsdl',
      array(
        'uri' => 'https://sws3-crt.cert.sabre.com',
        'location' => 'https://sws3-crt.cert.sabre.com',
        'stream_encoding(stream)' => 'utf-8',
        'trace' => true,
        'cache_wsdl' => WSDL_CACHE_NONE)
    );
    //Security Header array
    $security_header = array(
      'BinarySecurityToken' => $BinarySecurityToken,
    );
    try {
      $result = $client->__soapCall(
        'HotelRateDescriptionRQ',
        $service_array,
        null,
        array(
          $this->sabreSoapCreateMessageHeader('HotelRateDescriptionLLSRQ'),
          $this->createSecurityHeader($security_header)
        )
      );
    } catch (SoapFault $e) {
      var_dump($e);
    }

    return $result;

  }


   /***************************** BOOK ***************************************/

  /**
   * function sabreBookHotelReservation()
   *
   * @param (string) $BinarySecurityToken Sabre autentication's Security token
   * @param (int) $rph
   * @param (int) $number_units
   * @param (string) $guarantee
   */

  public function sabreBookHotelReservation($BinarySecurityToken, $rph, $number_units) {
    $service_array = array(
      'OTA_HotelResRQ' => array(
        'Version' => '2.2.0',
        'ReturnHostCommand' => 'true',
        'Hotel' => array(
          'BasicPropertyInfo' => array(
            'RPH' => $rph,
          ),
          'Guarantee' => array(
            'Type' => 'GDPST', //Value default
          ),
          'RoomType' => array(
            'NumberOfUnits' => $number_units,
          ),
        ),
      ),
    );
    $client = new SoapClient(
      'http://webservices.sabre.com/wsdl/tpfc/OTA_HotelResLLS2.2.0RQ.wsdl',
      array(
        'uri' => 'https://sws3-crt.cert.sabre.com',
        'location' => 'https://sws3-crt.cert.sabre.com',
        'stream_encoding(stream)' => 'utf-8',
        'trace' => true,
        'cache_wsdl' => WSDL_CACHE_NONE)
    );
    //Security Header array
    $security_header = array(
      'BinarySecurityToken' => $BinarySecurityToken,
    );
    try {
      $result = $client->__soapCall(
        'OTA_HotelResRQ',
        $service_array,
        null,
        array(
          $this->sabreSoapCreateMessageHeader('OTA_HotelResLLSRQ'),
          $this->createSecurityHeader($security_header)
        )
      );
    } catch (SoapFault $e) {
      var_dump($e);
    }
    return $result;
  }


  /**
  * function sabreModifyHotelReservation()
  * used to modify a confirmed hotel segment in a passenger name record (PNR)
  * @param String $BinarySecurityToken: token auth for soap API call.
  * @param String $criteria: array with params to update
  */
  public function sabreModifyHotelReservation($BinarySecurityToken, $criteria = array()) {
    $service_array = array(
      'HotelResModifyRQ' => array(
        'xmlns' => 'http://webservices.sabre.com/sabreXML/2011/10',
        'Version' => '2.1.0',
        'ReturnHostCommand' => 'true',
        'Hotel' => array(
          'BasicPropertyInfo' => array(
            'RPH' => '1',
            'ConfirmationNumber' => 'ABC123',
          ),
          'Guarantee' => array(
            'Type' => 'GDPST',
            /*'CC_Info' => array(
              'PaymentCard' => array(
                'Code' => 'AX',
                'ExpireDate' => '2020-12',
                'Number' => '1234567890',
              ),
              'PersonName' => array(
                'Surname' => 'TEST',
              ),
            ),*/
          ),
          'GuestCounts' => array(
            'Count' => '2',
          ),
          'RoomType' => array(
            'NumberOfUnits' => 1,
            'RoomTypeCode' => 'A2DRAC',
          ),
          'TimeSpan' => array(
            'End' => '07-20T13:00',
            'Start' => '07-19T12:00',
          ),
        ),
      ),
    );
    $client = new SoapClient(
      'http://wsdl-crt.cert.sabre.com/wsdl/tpfc/HotelResModifyLLS2.1.0RQ.wsdl',
      array(
        'uri' => 'https://sws3-crt.cert.sabre.com',
        'location' => 'https://sws3-crt.cert.sabre.com',
        'stream_encoding(stream)' => 'utf-8',
        'trace' => true,
        'cache_wsdl' => WSDL_CACHE_NONE)
    );
    //Security Header array
    $security_header = array(
      'BinarySecurityToken' => $BinarySecurityToken,
    );
    try {
      $result = $client->__soapCall(
        'HotelResModifyRQ',
        $service_array,
        null,
        array(
          $this->sabreSoapCreateMessageHeader('HotelResModifyLLSRQ'),
          $this->createSecurityHeader($security_header)
        )
      );
    } catch (SoapFault $e) {
      var_dump($e);
    }
    return $result;
  }

  /**
  *
  * HOTEL WORKFLOWS
  *
  */

  /**
  * function sabreWorkflowSearchHotel
  * workflow used for search hotel availability
  */
  public function sabreWorkflowSearchHotel($data) {

    $token_auth = $this->sabreSoapCreateSession();
    $response = $this->sabreSoapHotelAvailability($token_auth->BinarySecurityToken, $data);
    $this->sabreSoapCloseSession($token_auth->BinarySecurityToken);

    return $response;

  }

  /**
  * function sabreWorkflowHotelProperty
  * workflow used to return a hotel specific characteristics by hotel code
  */
  public function sabreWorkflowHotelProperty($data) {

    //set default vars
    $currency_code = 'COP';
    if (isset($data['currency_code'])) {
      $currency_code = $data['currency_code'];
    }
    $max_rate = '';
    if (isset($data['max_rate'])) {
      $max_rate = $data['max_rate'];
    }
    $min_rate = '';
    if (isset($data['min_rate'])) {
      $min_rate = $data['min_rate'];
    }


    $token_auth = $this->sabreSoapCreateSession();

    $response = $this->sabreHotelPropertyDescription($token_auth->BinarySecurityToken, $data['guests'], $data['hotel_code'], $data['end_date'], $data['start_date'], $currency_code, $max_rate, $min_rate);
    $this->sabreSoapCloseSession($token_auth->BinarySecurityToken);

    return $response;
  }

  public function sabreWorkflowHotelRateDescription($data) {
    //set default vars
    $currency_code = 'COP';
    if (isset($data['currency_code'])) {
      $currency_code = $data['currency_code'];
    }
    $max_rate = '';
    if (isset($data['max_rate'])) {
      $max_rate = $data['max_rate'];
    }
    $min_rate = '';
    if (isset($data['min_rate'])) {
      $min_rate = $data['min_rate'];
    }

    $token_auth = $this->sabreSoapCreateSession();

    $this->sabreHotelPropertyDescription($token_auth->BinarySecurityToken, $data['guests'], $data['hotel_code'], $data['end_date'], $data['start_date'], $currency_code, $max_rate, $min_rate);
    $response = $this->sabreHotelRateDescription($token_auth->BinarySecurityToken, $data['rph']);

    $this->sabreSoapCloseSession($token_auth->BinarySecurityToken);

    return $response;
  }

  public function sabreWorkflowHotelReservation($data) {
    $response = array();

    //Set pnr
    $user = new Users();
    $query_user = $user::find(
      array(
        'conditions' => "document = '" . $data['ps_document'] . "' AND document_type = '" . $data['ps_document_type'] . "'",
      )
    );
    if (count($query_user)) {
      $result = $query_user->getLast();
      $pnr = $result->id_pnr;
      $new_user = FALSE;
    } else {
      $pnr = '';
      $new_user = TRUE;
    }

    $currency_code = 'COP';
    if (isset($data['currency_code'])) {
      $currency_code = $data['currency_code'];
    }
    $max_rate = '';
    if (isset($data['max_rate'])) {
      $max_rate = $data['max_rate'];
    }
    $min_rate = '';
    if (isset($data['min_rate'])) {
      $min_rate = $data['min_rate'];
    }

    $token_auth = $this->sabreSoapCreateSession();

    $this->sabreHotelPropertyDescription($token_auth->BinarySecurityToken, $data['guests'], $data['hotel_code'], $data['end_date'], $data['start_date'], $currency_code, $max_rate, $min_rate);
    $this->sabreHotelRateDescription($token_auth->BinarySecurityToken, $data['rph']);
    $itinerary = new SabreItinerary();
    $itinerary->sabrePassengerDetails($token_auth->BinarySecurityToken, $data, $pnr);
    $response['Reservation'] = $this->sabreBookHotelReservation($token_auth->BinarySecurityToken, $data['rph'], $data['number_units']);
    $response['UserTransaction'] = $itinerary->sabreSoapEndTransaction($token_auth->BinarySecurityToken);
    $this->sabreSoapCloseSession($token_auth->BinarySecurityToken);
    if(isset($response['Reservation']->ApplicationResults->Success) && $new_user) {
      //call to User model
      $user->insertUser($data['ps_givenname'], $data['ps_surname'], $data['ps_document'], $data['ps_document_type'], $data['ps_email'], $data['ps_address'], $response['UserTransaction']->ItineraryRef->ID);
    }
    return $response;
  }


}
