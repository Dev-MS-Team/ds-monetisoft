<?php

class SabreItinerary extends Sabre {

  /**
  * function sabreTravelItineraryRead()
  * AAA session Required
  * @param String $BinarySecurityToken: token auth for soap API call
  * @return array $result: soap client result
  */
  public function sabreTravelItineraryRead($BinarySecurityToken, $unique_id = '', $subject_area = 'FULL') {
    $client = new SoapClient(
        "http://files.developer.sabre.com/wsdl/sabreXML1.0.00/pnrservices/TravelItineraryReadRQ3.6.0.wsdl",
        array(
          "uri" => 'https://sws3-crt.cert.sabre.com',
          "location" => 'https://sws3-crt.cert.sabre.com',
          "stream_encoding(stream)" => "utf-8",
          "trace" => true,
          'cache_wsdl' => WSDL_CACHE_NONE)
      );
      //Security Header array
      $security_header = array(
        'BinarySecurityToken' => $BinarySecurityToken,
      );
      try {
        $result = $client->__soapCall(
          "TravelItineraryReadRQ",
          $this->sabreSoapTravelItineraryReadBody('TravelItineraryReadRQ', $unique_id, $subject_area),
          null,
          array(
            $this->sabreSoapCreateMessageHeader("TravelItineraryReadRQ"),
            $this->createSecurityHeader($security_header)
          )
        );
      } catch (SoapFault $e) {
          var_dump($e);
      }
      return $result;
  }

  protected function sabreSoapTravelItineraryReadBody($actionString, $unique_id = '', $subject_area = 'FULL') {
    $result = array($actionString => array(
      "Version" => "3.6.0",
      "TimeStamp" =>  date("Y-m-d\TH:i:s-05:00"),//"2016-06-29T10:00:00-05:00"
      "xmlns" => "http://services.sabre.com/res/tir/v3_6",
      "MessagingDetails" => array(
        "SubjectAreas" => array(
            "SubjectArea" => $subject_area,
        ),
        "Transaction" => array(
          "Code" => "PNR"
        )
      )
    ));

    //if isn't empty unique id, then the uniqueID filter array is defined
    if (!empty($unique_id)) {
      $unique_array = array(
        "UniqueID" => array(
          "ID" => $unique_id,
        ),
      );

      $result[$actionString] = array_merge($result[$actionString], $unique_array);
    }

    return $result;

  }

  /**
  * function sabreOTA_Cancel()
  * is used to cancel itinerary segments contained within a passenger name record (PNR).
  * @param String $BinarySecurityToken: token auth for soap API call
  * @param String $type: is used to specify a particular segment type to cancel.
  * @param Int $end_number: is used to specify a range of segments to cancel.
  * @param Int $number: is used to specify a particular segment number to cancel.
  * @return array $result: soap client result
  */
  public function sabreOTA_Cancel($BinarySecurityToken, $type = 'entire', $end_number = '', $number = '') {
    $client = new SoapClient(
        "http://webservices.sabre.com/wsdl/tpfc/OTA_CancelLLS2.0.1RQ.wsdl",
        array(
          "uri" => 'https://sws3-crt.cert.sabre.com',
          "location" => 'https://sws3-crt.cert.sabre.com',
          "stream_encoding(stream)" => "utf-8",
          "trace" => true,
          'cache_wsdl' => WSDL_CACHE_NONE)
      );
      //Security Header array
      $security_header = array(
        'BinarySecurityToken' => $BinarySecurityToken,
      );
      try {
        $result = $client->__soapCall(
          "OTA_CancelRQ",
          $this->sabreSoapOTA_CancelBody('OTA_CancelRQ', $type, $end_number, $number),
          null,
          array(
            $this->sabreSoapCreateMessageHeader("OTA_CancelLLSRQ"),
            $this->createSecurityHeader($security_header)
          )
        );
      } catch (SoapFault $e) {
          var_dump($e);
      }
      return $result;
  }

  protected function sabreSoapOTA_CancelBody($actionString, $type, $end_number, $number) {

    $segment_array = array();
    if ($type != 'entire' && !empty($end_number) && !empty($number)) {
      $segment_array = array(
        "EndNumber" => $end_number,//"EndNumber" is used to specify a range of segments to cancel.
        "Number" => $number,//"Number" is used to specify a particular segment number to cancel.
      );
    }

    $result = array($actionString => array(
      "xmlns" => "http://webservices.sabre.com/sabreXML/2011/10",
      "NumResponses" => "1",
      "ReturnHostCommand" => "false",
      "TimeStamp" =>  date("Y-m-d\TH:i:s-05:00"),
      "Version" => "2.0.1",
      "Segment" => array(
        "Type" => $type, //"Type"  Acceptable values for "Type" are "air", "vehicle", "hotel", "other", or "entire".
        $segment_array
      ),
    ));
    return $result;
  }

  /**
  * function sabreSoapEndTransaction()
  * is used to complete and store changes made to a Passenger Name Record (PNR).
  * @param String $BinarySecurityToken: token auth for soap API call.
  * @return array $result: soap client response
  */
  public function sabreSoapEndTransaction($BinarySecurityToken) {
    $client = new SoapClient(
      "http://webservices.sabre.com/wsdl/tpfc/EndTransactionLLS2.0.5RQ.wsdl",
      array(
        "uri" => 'https://sws3-crt.cert.sabre.com',
        "location" => 'https://sws3-crt.cert.sabre.com',
        "stream_encoding(stream)" => "utf-8",
        "trace" => true,
        'cache_wsdl' => WSDL_CACHE_NONE)
      );
      //Security Header array
    $security_header = array(
      'BinarySecurityToken' => $BinarySecurityToken,
      );
    try {
      $result = $client->__soapCall(
        "EndTransactionRQ",
        $this->sabreSoapEndTransactionBody('EndTransactionRQ'),
        null,
        array(
          $this->sabreSoapCreateMessageHeader("EndTransactionLLSRQ"),
          $this->createSecurityHeader($security_header)
          )
        );
    } catch (SoapFault $e) {
      var_dump($e);
    }
    return $result;
  }

  protected function sabreSoapEndTransactionBody($actionString) {
    //Itinerary cannot combine with .../eTicket, or .../Invoice.
    $result = array($actionString => array(
      "xmlns" => "http://webservices.sabre.com/sabreXML/2011/10",
      "ReturnHostCommand" => "false",
      "TimeStamp" =>  date("Y-m-d\TH:i:s-05:00"),
      "Version" => "2.0.5",
      "EndTransaction" => array(
        "Ind" => "true",
        /*"Email" => array( //optional is used to send an email notification upon end transaction
          "Ind" => "true",
          "eTicket" => array( //optional  is used to send an email notification containing a PDF-based copy of the eTicket.
            "Ind" => "true",
            "PDF" => array(
              "Ind" => "true",
            )
          ),
          "Invoice" => array( //optional is used to send an email notification containing a text-based copy of the itinerary.
            "Ind" => "true",
          ),
          "Itinerary" => array( //optional. is used to send an email notification containing a text-based copy of the itinerary.
            "Ind" => "true",
            "PDF" => array(
              "Ind" => "true",
            ),
            "Segment" => array( // can be multiple
              "EndNumber" => "1", //is used to specify an ending segment in the range to include in the email message.
              "Number" => "2", // is used to specify a segment number to include in the email message.
            )
          ),
          "PersonName" => array( //optional
            "NameNumber" => "2.1" // "NameNumber" is used to specify a particular passenger name number to send the email notification to.
          ),
        ),*/
      ),
      "Source" => array(
        "ReceivedFrom" => "SWS TEST", // is used to receive the record.
      )
    ));
    return $result;
  }

  /**
   * @param String $BinarySecurityToken: token auth for soap API call
   * @param array $passenger_data
   * string [Phone]
   * string [PhoneUseType]
   * string [GivenName]
   * string [Surname]
   * string [Email]
   * @param int $uid unique id.
   * @param boolean $edit if is true, will edit the passenger details.
   * @return service response.
   */

  public function sabrePassengerDetails($BinarySecurityToken, $passenger_data, $uid = '', $edit = false) {

    $service_array = array(
      'PassengerDetailsRQ' => array(
        'xmlns' => 'http://services.sabre.com/sp/pd/v3_3',
        'version' => '3.3.0',
        'IgnoreOnError' => 'false',
        'HaltOnError' => 'false',
        'PostProcessing' => array(
          //'IgnoreAfter' => 'false',
          'RedisplayReservation' => 'false',
          //'UnmaskCreditCard' => 'true',
        ),
      ),
    );
    if ($edit && !empty($uid)) {
      $service_array['PassengerDetailsRQ'] += array(
        'PreProcessing' => array(
          'UniqueID' => array(
            'ID' => $uid,
          ),
        ),
        /*'SpecialReqDetails' => array(
          'SpecialServiceRQ' => array(
            'SpecialServiceInfo' => array(
              'AdvancePassenger' => array(
                'SegmentNumber' => $special_req_details['SegmentNumber'],
                'Document' => array(
                  'ExpirationDate' => $special_req_details['ExpirationDate'],
                  'Number' => $special_req_details['Number'],
                  'Type' => $special_req_details['Type'],
                  'IssueCountry' => $special_req_details['IssueCountry'],
                  'NationalityCountry' => $special_req_details['NationalityCountry'],
                ),
                'PersonName' => array(
                  'DateOfBirth' => $special_req_details['DateOfBirth'],
                  'Gender' => $special_req_details['Gender'],
                  'Namenumber' => $special_req_details['Namenumber'],
                  'DocumentHolder' => $special_req_details['DocumentHolder'],
                  'GivenName' => $special_req_details['GivenName'],
                  'MiddleName' => $special_req_details['MiddleName'],
                  'Surname' => $special_req_details['Surname'],
                ),
                /*'VendorPrefs' => array(
                  'Airline' => array(
                    'Hosted' => 'false'
                  ),
                ),
              ),
            ),
          ),
        ),*/
        'TravelItineraryAddInfoRQ' => array(
          /*'AgencyInfo' => array(
            'Address' => array(
              'AddressLine' => $travel_itinerary_info['AddressLine'],
              'CityName' => $travel_itinerary_info['CityName'],
              'CountryCode' => $travel_itinerary_info['CountryCode'],
              'PostalCode' => $travel_itinerary_info['PostalCode'],
              'StateCountyProv' => array(
                'StateCode' => $travel_itinerary_info['StateCode']
              ),
              'StreetNmbr' => $travel_itinerary_info['StreetNmbr'],
              'VendorPrefs' => array(
                'Airline' => array(
                  'Hosted' => $travel_itinerary_info['Hosted']
                ),
              ),
            ),
          ),*/
          'CustomerInfo' => array(
            'ContactNumbers' => array(
              'ContactNumber' => array(
                //'Namenumber' => $travel_itinerary_info['Namenumber'],
                'Phone' => $passenger_data['ps_phone'],
                //'PhoneUseType' => $passenger_data['ps_phone_use_type'],
              ),
            ),
            'PersonName' => array(
              'GivenName' => $passenger_data['ps_givenname'],
              'Surname' => $passenger_data['ps_surname'],
            ),
          ),
        ),
      );
    } else {
      if (!empty($uid)) {
        $service_array['PassengerDetailsRQ']['PreProcessing'] = array(
          'UniqueID' => array(
            'ID' => $uid,
          ),
        );
      } else {
        $service_array['PassengerDetailsRQ'] += array(
          'TravelItineraryAddInfoRQ' => array(
            'CustomerInfo' => array(
              'ContactNumbers' => array(
                'ContactNumber' => array(
                  'Phone' => $passenger_data['ps_phone'],
                  //"PhoneUseType" is used to specify if the number is agency, "A," home, "H," business, "B," or fax, "F."
                 // 'PhoneUseType' => $passenger_data['ps_PhoneUseType'],
                ),
              ),
              'PersonName' => array(
                'GivenName' => $passenger_data['ps_givenname'],
                'Surname' => $passenger_data['ps_surname'],
              ),
              'Email' => array(
                'Address' => $passenger_data['ps_email'],
              ),
            ),
          ),
        );
      }
    }

    $client = new SoapClient(
      'http://files.developer.sabre.com/wsdl/sabreXML1.0.00/ServicesPlatform/PassengerDetails3.3.0RQ.wsdl',
      array(
        'uri' => 'https://sws3-crt.cert.sabre.com',
        'location' => 'https://sws3-crt.cert.sabre.com',
        'stream_encoding(stream)' => 'utf-8',
        'trace' => true,
        'cache_wsdl' => WSDL_CACHE_NONE)
    );
    //Security Header array
    $security_header = array(
      'BinarySecurityToken' => $BinarySecurityToken,
    );
    try {
      $result = $client->__soapCall(
        'PassengerDetailsRQ',
        $service_array,
        null,
        array(
          $this->sabreSoapCreateMessageHeader('PassengerDetailsRQ'),
          $this->createSecurityHeader($security_header)
        )
      );
    } catch (SoapFault $e) {
      var_dump($e);
    }

    return $result;

  }

  /**
  * function sabreWorkflowCancelBook()
  * Workflow to cancel Itinerary segments
  * @param (array) $data: array with information related to cancel book segment
  */
  public function sabreWorkflowCancelBook($data) {

    if (!isset($data['unique_id']))
      return FALSE;

    //set default vars
    $type = 'entire';
    if (isset($data['type'])) {
      $type = $data['type'];
    }
    /*segments*/
    $number = '';
    if (isset($data['number'])) {
      $number = $data['number'];
    }
    $end_number = '';
    if (isset($data['end_number'])) {
      $end_number = $data['end_number'];
    }

    /*open session*/
    $token_auth = $this->sabreSoapCreateSession();
    /*travel itinerary read*/
    $response = $this->sabreTravelItineraryRead($token_auth->BinarySecurityToken, $data['unique_id']);
    /*cancel travel segment*/
    if (isset($response->ApplicationResults->Success))
    $response = $this->sabreOTA_Cancel($token_auth->BinarySecurityToken, $type, $end_number, $number);
    /*end and save transaction*/
    $this->sabreSoapEndTransaction($token_auth->BinarySecurityToken);
    /*close session*/
    $this->sabreSoapCloseSession($token_auth->BinarySecurityToken);

    return $response;

  }

}
