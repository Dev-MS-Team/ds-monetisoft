<?php

class Logisticapp extends BaseIntegrations {

  function __construct() {
    $config = parse_ini_file('logisticapp.ini');
    $this->rest_token = $config['REST_token'];
    $this->environment = $config['environment'];
  }

  /**
   * function quotationByProvider()
   * @param string $origin_address
   * @param float  $origin_latitude
   * @param float  $origin_longitude
   * @param string $destiny_laddress
   * @param float  $destiny_latitude
   * @param float  $destiny_longitude
   * @param string $declared_value
   * @param string $declared_weight
   */

  public function quotationByProvider($origin_address, $origin_latitude, $origin_longitude, $destiny_address, $destiny_latitude, $destiny_longitude, $declared_value = NULL, $declared_weight = NULL) {
    $headers = array(
      'Content-Type : application/x-www-form-urlencoded'
    );
    $path = $this->environment . '/shipping/quotationByProvider';
    $params = array(
      'gds_key' => $this->rest_token,
      'origin_address' => $origin_address,
      'origin_latitude' => $origin_latitude,
      'origin_longitude' => $origin_longitude,
      'destiny_laddress' => $destiny_address,
      'destiny_latitude' => $destiny_latitude,
      'destiny_longitude' => $destiny_longitude,
      'declared_value' => $declared_value,
      'declared_weight' => $declared_weight,
    );

    $result = $this->executePostCall($path, $params);
    return $result;
  }

/**
 * function postGds()
 * @param array $required_params
 * [gds_key]
 * [description_text]
 * [number_pieces]
 * [distance]
 * [origin_client]
 * [origin_address]
 * [origin_latitude]
 * [origin_longitude]
 * [destiny_address]
 * [destiny_latitude]
 * [destiny_longitude]
 * [amount]
 * [amount_declared]
 * [type_service]
 * [pay]
 * @param array $optional_params
 * [platform]
 * [width]
 * [height]
 * [long]
 * [weight]
 * [destiny_client]
 * [destiny_name]
 * [picture]
 * [content_pack]
 * [cellphone_destiny_client]
 * [email_destiny_client]
 * [bag_id]
 * [commerce_id]
 * [collect_payment]
 * [collect_amount]
 *
 * @return json
 */
  public function postGds($required_params, $optional_params = NULL) {
    $params = array(
      'gds_key' => $this->rest_token,
    );
    $path = $this->environment . '/shipping/postgds';

    $params = array_merge($params, $required_params);
    if (!is_null($optional_params)) {
      $params =array_merge($params, $optional_params);
    }

    $result = $this->executePostCall($path, $params);
    return $result;
  }

  /**
   *function trackShipping()
   * @param string $gds_key
   * @param string $commerce_id
   * @param string $shipping_id
   *
   */

  public function trackShipping($commerce_id, $shipping_id) {
    $path = $this->environment . '/shipping/trackShipping';
    $params = array(
      'gds_key' => $this->rest_token,
      'commerce_id' => $commerce_id,
      'shipping_id' => $shipping_id,
    );
    $result =$this->executePostCall($path, $params);
    return $result;
  }

  /**
   *function getShippingState()
   * @param string $gds_key
   * @param string $commerce_id
   * @param string $shipping_id
   *
   */

   public function getShippingState($commerce_id, $shipping_id) {
    $path = $this->environment . '/shipping/getShippingState';
    $params = array(
      'gds_key' => $this->rest_token,
      'commerce_id' => $commerce_id,
      'shipping_id' => $shipping_id,
    );
    $result =$this->executePostCall($path, $params);
    return $result;
  }

  /**
   * function providerBalance()
   * @param string $gds_key
   * @param int $commerce_id
   * @param int $provider_id
   *
   */

  public function providerBalance($commerce_id, $provider_id) {
    $path = $this->environment . '/balance/providerBalance';
    $params = array(
      'gds_key' => $this->rest_token,
      'commerce_id' => $commerce_id,
      'provider_id' => $provider_id,
    );
    $result = $this->executePostCall($path, $params);
    return $result;
  }

  /**
   *Function updateCommerce()
   *
   * @param string [gds_key] key de gds
   * @param array [commerce]
   *  int [commerce_id] Id de comercio
   *  string [commerce_name] Nombre Comercio
   *  bool int [state] Estado comercio 1 activo
   */
  public function updateCommerce($commerce_id, $commerce_name, $state) {
    $path = $this->environment . '/commerce/updatecommerce';
    $params = array(
      'gds_key' => $this->rest_token,
      'commerce' => array(
        'commerce_id' => $commerce_id,
        'commerce_name' => $commerce_name,
        'state' => $state,
      ),
    );
    $result = $this->executePostCall($path, $params);
    return $result;
  }
}


?>
