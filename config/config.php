<?php

return new \Phalcon\Config(array(
	'database' => array(
		'adapter'  => 'Mysql',
		'host'     => 'localhost',
		'username' => 'root',
		'password' => 'pass',
		'name'     => 'ms_ds',
	),
	'application' => array(
		'controllersDir' => __DIR__ . '/../app/controllers/',
		'modelsDir'      => __DIR__ . '/../app/models/',
		'baseUri'        => '/ds/',
	),
	'models' => array(
		'metadata' => array(
			'adapter' => 'Memory'
		)
	)
));

