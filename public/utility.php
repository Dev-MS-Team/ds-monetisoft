<?php
/**
* Requests for Utility API
*/
error_reporting(E_ALL);


use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Http\Request;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;


try {

  /**
   * Read the configuration
   */
  $config = include __DIR__.'/../config/config.php';

  $di = new FactoryDefault();

  /**
   * The URL component is used to generate all kind of urls in the application
   */
  $di->set('url', function() use ($config) {
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri($config->application->baseUri);
    return $url;
  });

  /**
   * Database connection is created based in the parameters defined in the configuration file
   */
  $di->set('db', function() use ($config) {
    return new Database(array(
      "host" => $config->database->host,
      "username" => $config->database->username,
      "password" => $config->database->password,
      "dbname" => $config->database->name
    ));
  });

  /**
   * Registering an autoloader
   */
  $loader = new Loader();

  $loader->registerDirs(
    array(
      $config->application->controllersDir . 'API/', //API CONTROLLERS
      $config->application->controllersDir, //CONTROLLERS ROOT DIRECTORY
      $config->application->controllersDir . 'integrations/', // INTEGRATIONS DIRECTORY
      $config->application->controllersDir . 'integrations/sabre', // SABRE INTEGRATION DIRECTORY
      $config->application->controllersDir . 'integrations/sabre/utility', //SABRE UTILITY CLASS
      $config->application->modelsDir, //MODELS ROOT DIRECTORY
      $config->application->modelsDir . 'integrations/sabre/', //SABRE MODELS
      $config->application->modelsDir . 'users/', //USERS MODELS DIRECTORY
      $config->application->modelsDir . 'customers/' //CUSTOMER MODELS
    )
  );

  //register libraries
  $loader->registerClasses(
    array(
        "XMLSerializer" => "../library/XMLSerializer.php",
    )
  );

  $loader->register();


  /**
   * Starting the application
   */
  $app = new Micro();
  $request = new Request();

  $access = new Account($request);
  $auth = $access->validateToken();

  /**
  * GET INFO AUTH FROM USER
  */
  if (!$auth['access']) {
    $app->response->setStatusCode(401, "Unauthorized")->sendHeaders();
    echo $auth['result'];
    exit();
  }


  /**
  * GEO AUTOCOMPLETE
  */
  $app->post('/api/v1/utility/geoautocomplete', function() use($request) {
    $output = array();
    $output['Status'] = TRUE;
    $output['Message'] = 'Success';

    //get vars
    $vars = $request->getPost();
    if (isset($vars['query']) && !empty($vars['query'])) {
      $query = str_replace(" ", ",", $vars['query']);

      $utility = new UtilitySabre();
      $result = $utility->sabreGoAutocomplete($query, 'CITY', 30);
      if (isset($result->Response->grouped->{'category:CITY'}->doclist->docs)) {
        $output['Result'] = $result->Response->grouped->{'category:CITY'}->doclist->docs;
      }
    } else {
      //query not found
      $output['Status'] = FALSE;
      $output['Message'] = 'Error';
    }
    echo json_encode($output);
  });


  $app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo 'This is crazy, but this page was not found!';
  });

  $app->handle();
} catch (\Exception $e) {
  echo "Exception: ", $e->getMessage();
}
 ?>
