<?php
/**
* Requests for hotels API V1
*/
error_reporting(E_ALL);


use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Http\Request;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;


try {

  /**
   * Read the configuration
   */
  $config = include __DIR__.'/../config/config.php';

  $di = new FactoryDefault();

  /**
   * The URL component is used to generate all kind of urls in the application
   */
  $di->set('url', function() use ($config) {
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri($config->application->baseUri);
    return $url;
  });

  /**
   * Database connection is created based in the parameters defined in the configuration file
   */
  $di->set('db', function() use ($config) {
    return new Database(array(
      "host" => $config->database->host,
      "username" => $config->database->username,
      "password" => $config->database->password,
      "dbname" => $config->database->name
    ));
  });

  /**
   * Registering an autoloader
   */
  $loader = new Loader();

  $loader->registerDirs(
    array(
      $config->application->controllersDir . 'API/', //API CONTROLLERS
      $config->application->controllersDir, //CONTROLLERS ROOT DIRECTORY
      $config->application->controllersDir . 'integrations/', // INTEGRATIONS DIRECTORY
      $config->application->controllersDir . 'integrations/sabre', // SABRE INTEGRATION DIRECTORY
      $config->application->modelsDir, //MODELS ROOT DIRECTORY
      $config->application->modelsDir . 'integrations/sabre/', //SABRE MODELS
      $config->application->modelsDir . 'users/', //USERS MODELS DIRECTORY
      $config->application->modelsDir . 'customers/' //CUSTOMER MODELS
    )
  );

  //register libraries
  $loader->registerClasses(
    array(
        "XMLSerializer" => "../library/XMLSerializer.php",
    )
  );

  $loader->register();


  /**
   * Starting the application
   */
  $app = new Micro();
  $request = new Request();

  $access = new Account($request);
  $auth = $access->validateToken();

  /**
  * GET INFO AUTH FROM USER
  */
  if (!$auth['access']) {
    $app->response->setStatusCode(401, "Unauthorized")->sendHeaders();
    echo $auth['result'];
    exit();
  }

  /**
  * SEARCH FOR HOTEL AVAILABLE
  */
  $controller = new Hotel($request);
  $app->post('/api/v1/hotels/avail', array($controller, 'HotelAvailability'));

  /**
  * GET HOTEL PROPERTIES FROM A SPECIFIC HOTEL
  */
  $app->post('/api/v1/hotels/properties', array($controller, 'HotelProperty'));

  /**
  *
  * GET HOTEL RATE DESCRIPTION
  */
  $app->post('/api/v1/hotels/rate', array($controller, 'HotelRateDescription'));

  /**
   * HOTEL BOOKING RESERVATION
   *
   */
  $app->post('/api/v1/hotels/booking', array($controller, 'HotelBooking'));

  $app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo 'This is crazy, but this page was not found!';
  });

  $app->handle();
} catch (\Exception $e) {
  echo "Exception: ", $e->getMessage();
}
 ?>
