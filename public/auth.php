<?php
/**
* Requests for authentication API
*/

error_reporting(E_ALL);


use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Http\Request;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;


try {

  /**
   * Read the configuration
   */
  $config = include __DIR__.'/../config/config.php';

  $di = new FactoryDefault();

  /**
   * The URL component is used to generate all kind of urls in the application
   */
  $di->set('url', function() use ($config) {
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri($config->application->baseUri);
    return $url;
  });

  /**
   * Database connection is created based in the parameters defined in the configuration file
   */
  $di->set('db', function() use ($config) {
    return new Database(array(
      "host" => $config->database->host,
      "username" => $config->database->username,
      "password" => $config->database->password,
      "dbname" => $config->database->name
    ));
  });

  /**
   * Registering an autoloader
   */
  $loader = new Loader();

  $loader->registerDirs(
    array(
      $config->application->controllersDir . 'integrations/sabre',
       $config->application->controllersDir . 'integrations/',
$config->application->modelsDir, //MODELS ROOT DIRECTORY
      $config->application->modelsDir . 'integrations/sabre/', //SABRE MODELS
      $config->application->modelsDir . 'users/', //USERS MODELS DIRECTORY


      $config->application->controllersDir . 'API/', //API CONTROLLERS
      $config->application->modelsDir . 'customers/' //CUSTOMER MODELS
    )
  );

  //register libraries
  $loader->registerClasses(
    array(
        "XMLSerializer" => "../library/XMLSerializer.php",
    )
  );

  $loader->register();


  /**
   * Starting the application
   */
  $app = new Micro();
  $request = new Request();

  /**
  * Service that generate the authentication token.
  */
  $controller = new Account($request);
  $app->post('/api/v1/auth/generate/token', array($controller, 'generateToken'));

  $app->get('/api/v1/auth/test', function () use ($request) {
   // print phpinfo();
    $sabre = new Sabre();
    print_r($sabre->authenticationSabre());

  });

  $app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo 'This page was not found!';
  });

  $app->handle();
} catch (\Exception $e) {
  echo "Exception: ", $e->getMessage();
}
 ?>
