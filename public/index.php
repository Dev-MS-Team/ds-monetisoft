<?php
error_reporting(E_ALL);

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Http\Request;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;


try {

  /**
   * Read the configuration
   */
  $config = include __DIR__.'/../config/config.php';

  $di = new FactoryDefault();

  /**
   * The URL component is used to generate all kind of urls in the application
   */
  $di->set('url', function() use ($config) {
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri($config->application->baseUri);
    return $url;
  });

  /**
   * Database connection is created based in the parameters defined in the configuration file
   */
  $di->set('db', function() use ($config) {
    return new Database(array(
      "host" => $config->database->host,
      "username" => $config->database->username,
      "password" => $config->database->password,
      "dbname" => $config->database->name
    ));
  });

  /**
   * Registering an autoloader
   */
  $loader = new Loader();

  $loader->registerDirs(
    array(
      $config->application->controllersDir,
      $config->application->controllersDir . 'integrations/',
      $config->application->controllersDir . 'integrations/sabre',
      $config->application->controllersDir . 'integrations/sabre/utility',
      $config->application->modelsDir,
      $config->application->modelsDir . 'integrations/sabre/',
    )
  );

  //register libraries
  $loader->registerClasses(
    array(
        "XMLSerializer" => "../library/XMLSerializer.php",
    )
  );

  $loader->register();


  /**
   * Starting the application
   */
  $app = new Micro();
  $request = new Request();


  $app->get('/', function() use ($app) {
    //insert wslog
    //$log = new WsLog();
    // $log->type = 'prueba';
    // $log->method = 'prueba';
    // $log->params = serialize(array('param'=> 'prueba'));
    // $log->result = serialize(array('response' => 'result'));
    // $log->timestamp = time();
    // print_r($log->save());
    // print_r($log->getLid());
    //print_r(time());
    //insert sabre_session

    $sabre = new SabreHotel();
    //print_r($sabre->sabreFlightsTo());
    $array1 = array(
     'SegmentNumber' => 'A',
     'ExpirationDate' => '2018-05-26',
     'Number' => '1234567890',
     'Type' => 'P',
     'IssueCountry' => 'FR',
     'NationalityCountry' => 'FR',
     'DateOfBirth' => '1980-12-02',
     'Gender' => 'M',
     'Namenumber' => '1.1',
     'DocumentHolder' => 'true',
     'GivenName' => 'JAMES',
     'MiddleName' => 'MALCOM',
     'Surname' => 'GREEN',
     'Hosted' => 'false',
   );
   $array2 = array(
     'AddressLine' => 'SABRE TRAVEL' ,
     'CityName' => 'SOUTHLAKE',
     'CountryCode' => 'US',
     'PostalCode' => '76092',
     'StateCode' => 'TX',
     'StreetNmbr' => '3150 SABRE DRIVE',
     'Hosted' => 'true',
     'Namenumber' => '1.1',
     'Phone' => '817-555-1212',
     'PhoneUseType' => 'H',
     'Namenumber' => '1.1',
     'NameReference' => 'ABC123',
     'PassengerType' => 'ADT',
     'GivenName' => 'MARCIN',
     'Surname' => 'LACHOWICZ',
   );
    //hotel availability search options
    $a = array();
    $a['guests'] = '2';
    $a['city_name'] = '';
    $a['country_code'] = '';
    $a['postal_code'] = '';
    $a['street'] = '';
    $a['city_code'] = 'BOG';
    $a['hotel_code'] = '';
    $a['hotel_name'] = '';
    $a['chain_code'] = '';
    $a['end_date'] = '07-20';
    $a['start_date'] = '07-19';

    //print_r($token_auth = $sabre->sabreSoapCreateSession());
    //print_r("<pre>");
    //print_r($sabre->sabrePassengerDetails($array1, $array2, '', $token_auth->BinarySecurityToken));
    //print_r($sabre->sabreBookHotelReservation($token_auth->BinarySecurityToken));
    //print_r($sabre->sabreOTA_Cancel($token_auth->BinarySecurityToken));
    //print_r($sabre->sabreSoapHotelAvailability($token_auth->BinarySecurityToken, $a));
    //print_r($sabre->sabreHotelPropertyDescription('2', '0017262', '07-20', '07-19', 'COP', '', '', $token_auth->BinarySecurityToken));
    //date_default_timezone_set("America/Bogota");
    //print_r($sabre->sabreTravelItineraryRead($token_auth->BinarySecurityToken, 'UBSFOG'));
    //print_r($sabre->sabreOTA_Cancel($token_auth->BinarySecurityToken));
    //print_r($sabre->sabreSoapEndTransaction($token_auth->BinarySecurityToken));
    //print_r("</pre>");
    //print_r($sabre->sabreSoapCloseSession($token_auth->BinarySecurityToken));
    /*print_r("<pre>");

    print_r("</pre>");
    print_r($sabre->sabreSoapCloseSession($token_auth->BinarySecurityToken));*/

    //print_r(date("Y-m-d\TH:i:s\Z"));
    //print_r($sabre->sabreSoapGetMessageHeaderXml('SessionCreateRQ'));

    // $sabre_sess->request_type = 'REST';
    // $sabre_sess->token_auth = 'sadsadxcnmxzbcdsadzxzxc';
    // $sabre_sess->token_expiration = time();
    // $sabre_sess->created = time();
    // $sabre_sess->save();
    // print_r($sabre_sess->getSid());
    $utility = new UtilitySabre();
    $utility = $utility->sabreGoAutocomplete($query = 'villa,de,leyva', $category = 'CITY', 30);
    print_r("<pre>");
    print_r($utility);
    print_r("</pre>");
  });

  $app->post('/api/v1/utility/geoautocomplete', function() use($request) {
    print_r($request->getPost());
  });


  $app->get('/api/robots', function(){
  });

  $app->get('/service', function() use($request) {
    //print_r($request->getHeaders());
    print phpinfo();
    $sabre = new Sabre();
    print_r($sabre->autentication_sabre());

    if ($request->isPost()) {
      //print_r($request->getPost());
        echo "post";
    } else if ($request->isGet()) {
      print_r($request->getHeaders());
    }
  });

  $app->post('/service', function() use($request) {
    //print_r($request->getHeaders());
    if ($request->isPost()) {
        print_r($request->getPost());
    } else if ($request->isGet()) {
      print_r($request->getHeaders());
    }
  });


  $app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo 'This is crazy, but this page was not found!';
  });

  $app->handle();
} catch (\Exception $e) {
  echo "Exception: ", $e->getMessage();
}
 ?>
