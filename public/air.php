<?php
/**
* AIR methods
*/
error_reporting(E_ALL);


use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Http\Request;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;


try {

  /**
   * Read the configuration
   */
  $config = include __DIR__.'/../config/config.php';

  $di = new FactoryDefault();

  /**
   * The URL component is used to generate all kind of urls in the application
   */
  $di->set('url', function() use ($config) {
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri($config->application->baseUri);
    return $url;
  });

  /**
   * Database connection is created based in the parameters defined in the configuration file
   */
  $di->set('db', function() use ($config) {
    return new Database(array(
      "host" => $config->database->host,
      "username" => $config->database->username,
      "password" => $config->database->password,
      "dbname" => $config->database->name
    ));
  });

  /**
   * Registering an autoloader
   */
  $loader = new Loader();

  $loader->registerDirs(
    array(
      $config->application->controllersDir,
      $config->application->controllersDir . 'integrations/',
      $config->application->modelsDir,
      $config->application->modelsDir . 'integrations/sabre/'
    )
  )->register();


  /**
   * Starting the application
   */
  $app = new Micro();
  $request = new Request();


  $app->get('/', function() use ($app) {
    //insert wslog
    //$log = new WsLog();
    // $log->type = 'prueba';
    // $log->method = 'prueba';
    // $log->params = serialize(array('param'=> 'prueba'));
    // $log->result = serialize(array('response' => 'result'));
    // $log->timestamp = time();
    // print_r($log->save());
    // print_r($log->getLid());
    //print_r(time());
    //insert sabre_session
    // $sabre_sess = new SabreSession();
    // $sabre_sess->request_type = 'REST';
    // $sabre_sess->token_auth = 'sadsadxcnmxzbcdsadzxzxc';
    // $sabre_sess->token_expiration = time();
    // $sabre_sess->created = time();
    // $sabre_sess->save();
    // print_r($sabre_sess->getSid());
  });


  $app->get('/api/robots', function(){
  });

  $app->get('/service', function() use($request) {
    //print_r($request->getHeaders());
    if ($request->isPost()) {
      //print_r($request->getPost());
        echo "post";
    } else if ($request->isGet()) {
      print_r($request->getHeaders());
    }
  });

  $app->post('/service', function() use($request) {
    //print_r($request->getHeaders());
    if ($request->isPost()) {
        print_r($request->getPost());
    } else if ($request->isGet()) {
      print_r($request->getHeaders());
    }
  });


  $app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo 'This is crazy, but this page was not found!';
  });

  $app->handle();
} catch (\Exception $e) {
  echo "Exception: ", $e->getMessage();
}
 ?>
